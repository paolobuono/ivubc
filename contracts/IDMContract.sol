pragma solidity >=0.4.22 <0.7.0;
pragma experimental ABIEncoderV2;
contract IDMContract{
    mapping(string => User) private users;
    mapping(string => Service) private services;
    event verification(string did,string status);
     constructor () public{
         string[] memory required=new string[](5);
         string[] memory toAddInfo=new string[](4);
         required[0]="null";
         required[1]="Nome";
         required[2]="Cognome";
         required[3]="Data di nascita";
         required[4]="Sesso";
         toAddInfo[0]="null";
         toAddInfo[1]="Nome scuola";
         toAddInfo[2]="Voto";
         toAddInfo[3]="Anno";
         
        Service diploma=new Service("Diploma",required ,toAddInfo);
        services["did:ethr:0xA0c24168517769dc56A066693c7Cb4f38313390A"]=diploma;
    }
    function addAddedInfoToUser(string memory did,string memory s_did,string[] memory infos)public{
        services[s_did].addServiceUserInfo(did,infos);
    }
    function getServiceInfo(string memory did,string memory pw,string memory s_did)public view returns(string[] memory,string[] memory){
        User user=users[did];
        require(user.getPassordHash()==keccak256(bytes(pw)));
        Service serv=services[s_did];
        return(serv.getRequiredInfo(),serv.getAddedInfo());
    }

    function getDiplomaCredendials(string memory did,string memory pw,string memory diploma_did) public view returns(bool,string[] memory){
        User user=users[did];
        require(user.getPassordHash()==keccak256(bytes(pw)));
        bool userBool=false;
        Service serv=services[diploma_did];
        string[] memory userInfo=serv.getUserInfo(did);
        if(userInfo.length!=0){userBool=true;}
        return (userBool,userInfo);
    }

    function getServiceName(string memory s_did)public view returns(string memory){
        Service serv=services[s_did];
        return serv.getServName();
    }
    function getServiceReqInfo(string memory s_did)public view returns(string[] memory){
        Service serv=services[s_did];
        return(serv.getRequiredInfo());
    }
function getServiceAddedInfo(string memory s_did)public view returns(string[] memory){
        Service serv=services[s_did];
        return(serv.getAddedInfo());
    }

    function addUserToService(string memory did, string memory pw,string memory s_did,string[] memory requestedInfo)public{
        User user=users[did];
        require(user.getPassordHash()==keccak256(bytes(pw)));
        string[] memory informations=new string[](requestedInfo.length);
        informations[0]="null";
        for(uint i=1; i<requestedInfo.length;i++){
            
            if(keccak256(abi.encodePacked(requestedInfo[i]))==keccak256(abi.encodePacked("Nome"))){informations[i]=user.getFirstName();}
            if(keccak256(abi.encodePacked(requestedInfo[i]))==keccak256(abi.encodePacked("Cognome"))){informations[i]=user.getLastName();}
            if(keccak256(abi.encodePacked(requestedInfo[i]))==keccak256(abi.encodePacked("E-mail"))){informations[i]=user.getEmail();}
            if(keccak256(abi.encodePacked(requestedInfo[i]))==keccak256(abi.encodePacked("Tel"))){informations[i]=user.getMobile();}
            if(keccak256(abi.encodePacked(requestedInfo[i]))==keccak256(abi.encodePacked("Data di nascita"))){informations[i]=user.getDOB();}
            if(keccak256(abi.encodePacked(requestedInfo[i]))==keccak256(abi.encodePacked("Sesso"))){informations[i]=user.getGender();}
        }
        
        services[s_did].addUser(did,informations);
        
        emit verification(did,"information retrieved");
    }
    function createUser(string memory first_name,string memory last_name,string memory password,string memory mobile, string memory gen,string memory email,string memory DOB,string memory DID) public{
        
        User user=new User(first_name,password);
        user.setLastName(last_name);
        user.setGender(gen);
        user.setEmail(email);
        user.setDOB(DOB);
        user.setDID(DID);
        user.setMobile(mobile);
        users[DID]=user;
    }

    function editUser(string memory first_name,string memory last_name,string memory password,string memory mobile,string memory gen, string memory email,string memory DOB,string memory DID) public{
        User user=users[DID];
        require(user.getPassordHash()==keccak256(bytes(password)));
        users[DID].setFirstName(first_name);
        users[DID].setLastName(last_name);
        users[DID].setEmail(email);
        users[DID].setMobile(mobile);
        users[DID].setGender(gen);
        users[DID].setDOB(DOB);
    }
/*
    function hash(string memory name,string memory did,string memory dob) private view returns (bytes32){
            string memory full_s=string(abi.encodePacked(name,did,dob));
            bytes memory full_b=bytes(full_s);
            return keccak256(full_b);
    }
    */
    function getDetails(string memory did, string memory pw) public view returns(string memory ,string memory,string memory ,string memory, string memory,string memory){
        User user = users[did];
      require(user.getPassordHash()==keccak256(bytes(pw)));

        return (user.getFirstName(),user.getLastName(),user.getEmail(),user.getMobile(),user.getDOB(),user.getGender());
    }
}
contract Service{
string serv_name;
string[] requiredInfo;
string[] addedInfo;
mapping (string => string[]) users;

constructor (string memory service_name,string[] memory required,string[] memory toAddInfo) public{
        serv_name=service_name;
        requiredInfo=required;
        addedInfo=toAddInfo;
    }
function getServName() public view returns(string memory){
        return serv_name;
    }
function getRequiredInfo() public view returns(string[] memory){
    return requiredInfo;
}
function getAddedInfo() public view returns(string[] memory){
    return addedInfo;
}
function addUser(string memory user_did, string[] memory userInfo)public{
    users[user_did]=userInfo;
    }
function addServiceUserInfo(string memory user_did,string[] memory infos)public{
    for(uint i=0;i<infos.length;i++){
    users[user_did].push(infos[i]);
    }
}
function getUserInfo(string memory user_did) public view returns(string[] memory){
    return users[user_did];
}
}

contract User{
    // true for User, false for serviceProvider
    string first_name;
    string last_name;
    string passwordHash;
    string email;
    string mobile;
    string DOB;
    string gender;
    string DID;

    
    constructor(string memory n, string memory password) public{
        first_name = n;
        passwordHash = password;
    }


function setFirstName(string memory fn)public {
first_name=fn;
}
function setLastName(string memory ln)public {
    last_name=ln;
}
function setDID(string memory d_id) public{
    DID=d_id;
} 
function setMobile(string memory tel) public{
    mobile=tel;
}
function setEmail(string memory em) public{
        email = em;
    }
    function setDOB(string memory dob) public {
        DOB = dob;
    }
    function setGender(string memory gen) public{
        gender=gen;
    }
    
    function getGender() public view returns(string memory){
        return gender;
    }
    
    function getPassordHash() public view returns(bytes32){
        return (keccak256(bytes(passwordHash)));
    }
    function getFirstName() public view returns(string memory){
        return first_name;
    }
    function getLastName() public view returns(string memory){
        return last_name;
    }
    function getEmail() public view returns (string memory){
        return email;
    }
    function getDOB() public view returns( string memory){
        return DOB;
    }
    function getDID() public view returns(string memory){
        return DID;
    }
    function getMobile() public view returns(string memory){
        return mobile;
    }

    function bytes32toString(bytes32 _bytes32) public view returns(string memory){
        bytes memory bytesArray = new bytes(32);
        for(uint256 i; i<32;i++){
            bytesArray[i]=_bytes32[i];
        }
        return string(bytesArray);
    }
}