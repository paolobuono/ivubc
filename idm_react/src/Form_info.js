import React from 'react';
import { ABI } from './config'
import Web3 from 'web3'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import {Succeed} from './succeed'
import {Formik, Field, Form} from 'formik'
import {useState} from 'react'
export const Form_info = (props) => {
    const [succeed, setSucceed] = useState(false)
    if(succeed){
        return(<Succeed></Succeed>)
    }
    const init = async (values) => {
    const HDWalletProvider =require('@truffle/hdwallet-provider')
    const address='0x09beC456F776C714600C162F52C28190641371E7'
    const privateKey='0811c0b1de411e956bbd9c51d8496ffee2e07eefec8c6e6ec750066d49086377'
    const provider= new HDWalletProvider(privateKey,'https://kovan.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32')
    const web3 = new Web3(provider)
    const IDMContract = new web3.eth.Contract(ABI, '0xeCB429D2c41f5410e55B351d221b17AF69FC0827')
    const diploma_did="did:ethr:0xA0c24168517769dc56A066693c7Cb4f38313390A"
    var infos=[]
    infos[0]=values.nome_scuola
    infos[1]=values.voto
    infos[2]=values.anno
    //IDMContract.methods.addAddedInfoToUser(props.userDid,diploma_did,infos).send()
    IDMContract.methods.addAddedInfoToUser(props.userDid,diploma_did,infos).send({ from: address}).on('transactionHash', function(hash){
        console.log('transactionHash');
    })
    .on('receipt', function(receipt){
            setSucceed(true)
    })
    .on('confirmation', function(confirmationNumber, receipt) {
    })
    .on('error', console.error);
    }

    return (
        
        <Formik
            initialValues={{ nome_scuola: '', voto: '', anno: '' }}
            onSubmit={(values, { setSubmitting }) => {
                init(values)
            }}
        >
            {({ isSubmitting }) => (
                <Form>
                    <div className="form-group">
                        <label htmlFor="nome_scuola">Nome università:</label>
                        <Field name="nome_scuola" placeholder="Inserisci il nome dell'università" className="form-control" type="text" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="voto">Voto finale:</label>
                        <Field name="voto" placeholder="Inserisci il voto finale" className="form-control" type="text" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="anno">Anno conseguimento:</label>
                        <Field name="anno" placeholder="Inserisci l'anno di conseguimento del titolo" className="form-control" type="text" />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary" disabled={isSubmitting}>{isSubmitting ? "Caricamento..." : "Submit"}</button>
                    </div>

                </Form>
            )}
        </Formik>
    );
};