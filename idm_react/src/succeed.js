import React, { Component } from 'react';
import img1 from './assets/done.png';
export const Succeed = () => {
    return(
    <div align="center">
        <h1>Congratulazioni!</h1><br/>
        <h3>Hai ottenuto il tuo diploma</h3><br/>
        <img src={img1}/>
    </div>   
);
    };