import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { About } from './About';
import { Contact } from './Contact';
import { Start } from './Start';
import { Layout } from './components/Layout';
import { Jumbotron } from './components/Jumbotron';
import Card from 'react-bootstrap/Card'
import { Nav} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import img1 from './assets/blockchain.jpg';
import { DiplomaCard } from './components/DiplomaCard';
import GetDiploma from './GetDiploma'
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <Jumbotron />
          <Layout>
            <Switch>
            <Route exact path="/" component={Start} />
              <Route path="/GetDiploma" component={GetDiploma} />
              <Route path="/contact" component={Contact} />
            </Switch>
          </Layout>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
