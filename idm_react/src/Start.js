import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { About } from './About';
import { Contact } from './Contact';
import { Layout } from './components/Layout';
import { Jumbotron } from './components/Jumbotron';
import { DiplomaCard } from './components/DiplomaCard';
import { Succeed } from './succeed';

  export const Start = () => (
    <div>
      <DiplomaCard />
    </div>
    )