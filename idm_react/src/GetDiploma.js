import React, { Component, Fragment } from 'react';
import Web3 from 'web3'
import Row from 'react-bootstrap/Row'
import { ABI } from './config'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import { Start } from './Start'
import {Form_info} from './Form_info'
import {Succeed} from './succeed'
var QRCode = require('qrcode.react')
class GetDiploma extends Component {
  constructor(props) {
    super(props)
    this.state = {
      did: '',
      user_did: '',
      required_info: [],
      added_info: [],
      tasks: [],
      verified: false,
      succeed:false,
      loading: true
    }
  }
  /*
  componentDidMount(){
    const web3 = new Web3(Web3.givenProvider || "https://ropsten.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32")
    const IDMContract = new web3.eth.Contract(ABI, '0xdA724252cD00A2f4de12885Dc73fcbf806ef1395')
    IDMContract.events.verification().watch(function(error, result) {
      if(!error){
          console.log("Event captured:", result);
      }
      else{
          console.log("Error capturing event:", error);
      }
  });

  }
  */

 onChangeValueHandler = (val) => {
  this.setState({ succeed: true })
}
  componentWillMount() {
    const web3 = new Web3(Web3.givenProvider || "https://kovan.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32")
    const IDMContract = new web3.eth.Contract(ABI, '0xeCB429D2c41f5410e55B351d221b17AF69FC0827')
    const diploma_did="did:ethr:0xA0c24168517769dc56A066693c7Cb4f38313390A"
    this.setState({did: 'did:ethr:0xA0c24168517769dc56A066693c7Cb4f38313390A'})
    const getReqInfo=IDMContract.methods.getServiceReqInfo(diploma_did).call().then((v) => this.setState({required_info: v}))
    const getAddedInfo=IDMContract.methods.getServiceAddedInfo(diploma_did).call().then((v2) => this.setState({added_info: v2}))
    const web3socket = new Web3(new Web3.providers.WebsocketProvider("wss://kovan.infura.io/ws/v3/030c2d8a20e54679b2cde8e33b03cd32"))
    const socketInstance =new web3socket.eth.Contract(ABI,'0xeCB429D2c41f5410e55B351d221b17AF69FC0827');
    socketInstance.events.verification((err, events)=>{
    console.log(err, events)
      this.setState({verified:true})
      this.setState({user_did:events.returnValues[0]})
  })

}
  render() {
    var qrcode_text="DID: '".concat(this.state.did).concat("'/ReqInfo:").concat(this.state.required_info).concat("/AddedInfo:").concat(this.state.added_info)
    //var taskStatus = this.props.verificated;
    if(this.state.verified)
        return <Form_info userDid={this.state.user_did}></Form_info>
    return (
    
<Fragment>
      <h3>Per ottenere il tuo diploma digitale scannerizza il seguente QR code con l'app </h3>

      <Container>
  <Row>
    <Col>
      <QRCode value={qrcode_text} size="248" />
      </Col>
    <Col>
    <br/><br/>
    <h5>Informazioni richieste:</h5><br/>
    <ul>
      {
      this.state.required_info.map((value, index) => {
        if(index!=0){return <li key={index}>{value}</li>}
      })}
    </ul>

    </Col>
  </Row>
</Container>
      </Fragment>
      
    );
    }
}
export default GetDiploma