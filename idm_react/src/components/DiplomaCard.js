import React from 'react';
import { Container } from 'react-bootstrap';
import GetDiploma from '../GetDiploma'
import {Form_info} from '../Form_info'
export const DiplomaCard = () => (
    <div align="center">

  <div align="center" class="card w-75">
<div align="center" class="card text-center" >
  <div class="card-header">
    Provider: Università
  </div>
  <div class="card-body">
    <h5 class="card-title">Diploma</h5>
    <p class="card-text">Ottieni una copia digitale del tuo diploma, per condividerlo in una domanda post-laurea o al prossimo colloquio di lavoro.</p>
    <a href="../GetDiploma " class="btn btn-primary">Ottieni il diploma</a>
  </div>
  <div class="card-footer text-muted">
    Informazioni richieste: Nome, Cognome, Data di nascita, Sesso
  </div>
</div>
</div>
  </div>
)
