package com.example.idm;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.infura.InfuraHttpService;
import org.web3j.tuples.generated.Tuple5;
import org.web3j.tuples.generated.Tuple6;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.DefaultGasProvider;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
public class Identity_view extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;




    String contractAddress = "0xeCB429D2c41f5410e55B351d221b17AF69FC0827";
    Web3j web3 = Web3j.build(new InfuraHttpService("https://kovan.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32"));
    final BigInteger gasLimit = BigInteger.valueOf(5500000);
    final BigInteger gasPrice = BigInteger.valueOf(20_000_000_000L);
    SharedPreferences preferences;
    Credentials credentials = Credentials.create("0811c0b1de411e956bbd9c51d8496ffee2e07eefec8c6e6ec750066d49086377");
    FloatingActionButton fab;
    TextView nome,cognome, mail, tel, dob, gen;
    EditText ed_nome,ed_cognome, ed_mail, ed_tel;
    Button data;
    ToggleButton toggle;
    RadioButton radioF, radioM;
    ToggleButton edit;
    String genere;
    DatePickerDialog dob_picker;
    Boolean edited=false;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onPause(){
    super.onPause();
        if(edited){
            final IDMContract idm = IDMContract.load(contractAddress, web3, credentials, gasPrice, gasLimit);
            Future<TransactionReceipt> transactionReceipt =idm.editUser(nome.getText().toString(),cognome.getText().toString(), preferences.getString("password", ""), tel.getText().toString(), genere, mail.getText().toString(), dob.getText().toString(), "did:ethr:" + preferences.getString("address", "")).sendAsync();
            try {
                Log.d("log", transactionReceipt.get().getTransactionHash());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identity_view);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Identità");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.close_str);
        drawer.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fab=findViewById(R.id.fab);
        nome = (TextView) findViewById(R.id.vnome);
        cognome=(TextView) findViewById(R.id.vcognome);
        mail = (TextView) findViewById(R.id.vmail);
        tel = (TextView) findViewById(R.id.vtel);
        dob = (TextView) findViewById(R.id.vdob);
        gen = (TextView) findViewById(R.id.vgen);
        toggle = findViewById(R.id.toggleButton);
        ed_nome = findViewById(R.id.editNome);
        ed_cognome=findViewById(R.id.editCognome);
        ed_mail = findViewById(R.id.editMail);
        ed_tel = findViewById(R.id.editTel);
        data = findViewById(R.id.buttonData);
        radioF = findViewById(R.id.radioF);
        radioM = findViewById(R.id.radioM);
        String walletDirectory = getApplicationContext().getFilesDir().toString();
        ed_nome.setVisibility(View.GONE);
        ed_cognome.setVisibility(View.GONE);
        ed_mail.setVisibility(View.GONE);
        ed_tel.setVisibility(View.GONE);
        data.setVisibility(View.GONE);
        radioM.setVisibility(View.GONE);
        radioF.setVisibility(View.GONE);
        preferences = getSharedPreferences("cr", MODE_PRIVATE);


        final IDMContract idm = IDMContract.load(contractAddress, web3, credentials, gasPrice, gasLimit);
        Future<Tuple6<String,String, String, String, String, String>> details = idm.getDetails("did:ethr:" + preferences.getString("address", ""), preferences.getString("password", "")).sendAsync();
        try {
            nome.setText(details.get().component1());
            cognome.setText(details.get().component2());
            mail.setText(details.get().component3());
            tel.setText(details.get().component4());
            dob.setText(details.get().component5());
            gen.setText(details.get().component6());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        bottone.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                final IDMContract idm = IDMContract.load(contractAddress, web3, credentials, gasPrice, gasLimit);
                Future<Tuple5<String, String, String, String, String>> details = idm.getDetails("did:ethr:" + preferences.getString("address", ""), preferences.getString("password", "")).sendAsync();
                try {
                    nome.setText(details.get().component1());
                    mail.setText(details.get().component2());
                    tel.setText(details.get().component3());
                    dob.setText(details.get().component4());
                    gen.setText(details.get().component5());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        */
        radioF.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v){
                radioF.setChecked(true);
                radioM.setChecked(false);
                genere="Donna";
            }
        });
        radioM.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v){
                radioM.setChecked(true);
                radioF.setChecked(false);
                genere="Uomo";
            }
        });
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggle.isChecked()){
                    nome.setVisibility(View.GONE);
                    cognome.setVisibility(View.GONE);
                    mail.setVisibility(View.GONE);
                    tel.setVisibility(View.GONE);
                    dob.setVisibility(View.GONE);
                    gen.setVisibility(View.GONE);

                    ed_nome.setVisibility(View.VISIBLE); ed_nome.setText(nome.getText());
                    ed_cognome.setVisibility(View.VISIBLE); ed_cognome.setText(cognome.getText());
                    ed_mail.setVisibility(View.VISIBLE);ed_mail.setText(mail.getText());
                    ed_tel.setVisibility(View.VISIBLE);ed_tel.setText(tel.getText());
                    data.setVisibility(View.VISIBLE);
                    radioF.setVisibility(View.VISIBLE);
                    radioM.setVisibility(View.VISIBLE);

                }else{
                    edited=true;
                    nome.setVisibility(View.VISIBLE);
                    cognome.setVisibility(View.VISIBLE);
                    mail.setVisibility(View.VISIBLE);
                    tel.setVisibility(View.VISIBLE);
                    dob.setVisibility(View.VISIBLE);
                    gen.setVisibility(View.VISIBLE);

                    ed_nome.setVisibility(View.GONE);
                    ed_cognome.setVisibility(View.GONE);
                    ed_mail.setVisibility(View.GONE);
                    ed_tel.setVisibility(View.GONE);
                    data.setVisibility(View.GONE);
                    radioF.setVisibility(View.GONE);
                    radioM.setVisibility(View.GONE);

                    nome.setText(ed_nome.getText());
                    mail.setText(ed_mail.getText());
                    tel.setText(ed_tel.getText());
                    if(radioF.isChecked()||radioM.isChecked()){
                    gen.setText(genere);
                    }
                }
            }
        });

        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                final int day = cldr.get(Calendar.DAY_OF_MONTH);
                final int month = cldr.get(Calendar.MONTH);
                final int year_ = cldr.get(Calendar.YEAR);
                // date picker dialog
                dob_picker = new DatePickerDialog(Identity_view.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year_, month, day);
                dob_picker.show();
            }
        });
fab.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        try {

            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

            startActivityForResult(intent, 0);

        } catch (Exception e) {

            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);

        }
    }
});

    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                if (resultCode == RESULT_CANCELED) {
                    //handle cancel
                }
                String contents = data.getStringExtra("SCAN_RESULT");
                Intent i=new Intent(Identity_view.this,Verif_activity.class);
                i.putExtra("scan_result",contents);
                startActivity(i);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:

                i = new Intent(this, com.example.idm.Credentials.class);
                startActivity(i);
                break;

            case R.id.nav_identity:
                break;
            case R.id.nav_settings:
                i = new Intent(this, Settings.class);
                startActivity(i);
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    /*
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:

                i = new Intent(this, Credentials.class);
                startActivity(i);
                break;

            case R.id.nav_identity:
                i = new Intent(this, Identity_view.class);
                startActivity(i);
                break;
            case R.id.nav_settings:
                i = new Intent(this, Settings.class);
                startActivity(i);
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    */
}
