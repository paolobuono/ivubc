package com.example.idm;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.infura.InfuraHttpService;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Settings extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    FloatingActionButton fab;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Impostazioni");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.close_str);
        drawer.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fab=findViewById(R.id.fab);
        SharedPreferences preferences = getSharedPreferences("cr", MODE_PRIVATE);
        ListView listview=findViewById(R.id.listview);
        List<HashMap<String,String>> lista=new ArrayList<HashMap<String, String>>();
        HashMap<String,String> map1=new HashMap<String,String>();
        map1.put("Title","DID");
        map1.put("Description","did:ethr:" + preferences.getString("address", ""));
        lista.add(map1);
        HashMap<String,String> map2=new HashMap<String,String>();
        map2.put("Title","Rete");
        map2.put("Description","Ropsten");
        lista.add(map2);
        String[] from={"Title","Description"};
        int[] to={R.id.title,R.id.description};
        SimpleAdapter adapter=new SimpleAdapter(getBaseContext(),lista,R.layout.row2,from,to);
        listview.setAdapter(adapter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

                    startActivityForResult(intent, 0);

                } catch (Exception e) {

                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
                    startActivity(marketIntent);

                }
            }
        });
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:

                i = new Intent(this, com.example.idm.Credentials.class);
                startActivity(i);
                break;

            case R.id.nav_identity:
                i = new Intent(this, Identity_view.class);
                startActivity(i);
                break;
            case R.id.nav_settings:
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                if (resultCode == RESULT_CANCELED) {
                    //handle cancel
                }
                String contents = data.getStringExtra("SCAN_RESULT");
                Intent i=new Intent(Settings.this,Verif_activity.class);
                i.putExtra("scan_result",contents);
                startActivity(i);
            }
        }
    }
}