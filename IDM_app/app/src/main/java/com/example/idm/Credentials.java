package com.example.idm;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.infura.InfuraHttpService;
import org.web3j.tuples.generated.Tuple2;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Credentials extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    SharedPreferences preferences;
    CardView diplomaCard;
    DrawerLayout drawer;
    FloatingActionButton fab;
    NavigationView navigationView;
    Toolbar toolbar=null;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Credenziali");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.close_str);
        drawer.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fab=findViewById(R.id.fab);
        diplomaCard=findViewById(R.id.cardDiploma);
        diplomaCard.setVisibility(View.GONE);
        boolean diplomaBool;
        String contractAddress = "0xeCB429D2c41f5410e55B351d221b17AF69FC0827";
        Web3j web3 = Web3j.build(new InfuraHttpService("https://kovan.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32"));
        final BigInteger gasLimit = BigInteger.valueOf(5500000);
        final BigInteger gasPrice = BigInteger.valueOf(20_000_000_000L);
        preferences = getSharedPreferences("cr", MODE_PRIVATE);
        org.web3j.crypto.Credentials credentials = org.web3j.crypto.Credentials.create("0811c0b1de411e956bbd9c51d8496ffee2e07eefec8c6e6ec750066d49086377");
        final IDMContract idm = IDMContract.load(contractAddress, web3, credentials, gasPrice, gasLimit);
        Future<Tuple2<Boolean, List<String>>> diplomaCred=idm.getDiplomaCredendials("did:ethr:" + preferences.getString("address", ""), preferences.getString("password", ""),"did:ethr:0xA0c24168517769dc56A066693c7Cb4f38313390A").sendAsync();

        try {
            diplomaBool=diplomaCred.get().component1();
            if(diplomaBool)diplomaCard.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        diplomaCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("prova","vemo");
                Intent i=new Intent(com.example.idm.Credentials.this,Info_view.class);
                startActivity(i);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

                    startActivityForResult(intent, 0);

                } catch (Exception e) {

                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
                    startActivity(marketIntent);

                }
            }
        });
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:
                break;

            case R.id.nav_identity:
                i = new Intent(this, Identity_view.class);
                startActivity(i);
                break;
            case R.id.nav_settings:
                i = new Intent(this, Settings.class);
                startActivity(i);
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
        }
        else{
            CharSequence text = "A presto!";
            Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                if (resultCode == RESULT_CANCELED) {
                    //handle cancel
                }
                String contents = data.getStringExtra("SCAN_RESULT");
                Intent i=new Intent(com.example.idm.Credentials.this,Verif_activity.class);
                i.putExtra("scan_result",contents);
                startActivity(i);
            }
        }
    }
}