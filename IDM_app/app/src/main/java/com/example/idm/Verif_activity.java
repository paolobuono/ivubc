package com.example.idm;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.infura.InfuraHttpService;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.concurrent.Future;

public class Verif_activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    ProgressDialog progress;
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    FloatingActionButton fab;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verif_activity);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ottieni diploma");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_open, R.string.close_str);
        drawer.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fab=findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        TextView tv_nome=findViewById(R.id.textVNome);
        ListView listView = (ListView)findViewById(R.id.listview);
        Button button=findViewById(R.id.button);
        String contractAddress = "0xeCB429D2c41f5410e55B351d221b17AF69FC0827";
        String infuraWS = "wss://ropsten.infura.io/ws/v3/030c2d8a20e54679b2cde8e33b03cd32";

        Web3j web3 = Web3j.build(new InfuraHttpService("https://kovan.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32"));
        final BigInteger gasLimit = BigInteger.valueOf(5500000);
        final BigInteger gasPrice = BigInteger.valueOf(20_000_000_000L);
        SharedPreferences preferences = getSharedPreferences("cr", MODE_PRIVATE);
        Credentials credentials = Credentials.create("0811c0b1de411e956bbd9c51d8496ffee2e07eefec8c6e6ec750066d49086377");
        Intent i=getIntent();
        String[] det = i.getStringExtra("scan_result").split("/");
        String[] did_arr = det[0].split("'");//did=did_arr[1]
        String[] reqinf_arr = det[1].split(":");
        reqinf_arr=reqinf_arr[1].split(",");
        String[] requestedInfo=new String[reqinf_arr.length-1];
        for(int k=0;k<reqinf_arr.length-1;k++){
            requestedInfo[k]=reqinf_arr[k+1];
        }
        Log.d("proviamo",requestedInfo.toString());
        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this, R.layout.row, R.id.description, requestedInfo);
        listView.setAdapter(arrayAdapter);
        final IDMContract idm = IDMContract.load(contractAddress, web3, credentials, gasPrice, gasLimit);
        Future<String> servName=idm.getServiceName(did_arr[1]).sendAsync();
        try {
            tv_nome.setText(servName.get().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void run() {

                Future<TransactionReceipt> transactionReceipt =idm.addUserToService("did:ethr:" + preferences.getString("address", ""),preferences.getString("password", ""),did_arr[1], Arrays.asList(requestedInfo)).sendAsync();
                try {
                    String result = transactionReceipt.get().getTransactionHash() + " " + transactionReceipt.get().getGasUsed();
                    Log.d("prova", result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
*/

        String[] finalReqinf_arr = reqinf_arr;
        button.setOnClickListener(new View.OnClickListener() {
    @SuppressLint("CheckResult")
    @Override
    public void onClick(View view) {
         progress = new ProgressDialog(Verif_activity.this);
        progress.show();
        progress.setContentView(R.layout.progress_dialog);
        progress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        new Thread(new Runnable() {
            @SuppressLint("CheckResult")
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void run() {

                Future<TransactionReceipt> transactionReceipt =idm.addUserToService("did:ethr:" + preferences.getString("address", ""),preferences.getString("password", ""),did_arr[1], Arrays.asList(finalReqinf_arr)).sendAsync();
                try {
                    String result = transactionReceipt.get().getTransactionHash() + " " + transactionReceipt.get().getGasUsed();
                    Log.d("prova", result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progress.dismiss();
            }
        }).start();

        /*
        EthFilter filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, idm.getContractAddress().substring(2));
        web3WS.ethLogFlowable(filter).subscribe(event ->{
            Log.d("info", String.valueOf(event));
                },Throwable ->{

        });

        idm.verificationEventFlowable(filter)
                    .subscribe(event -> {

                        final String event_did = event.did;
                        final String event_status = event.status;
                        Log.d("dentro","sonodentro");
                        if (event_status.equals("verification succeed") && event_did.equals("did:ethr:" + preferences.getString("address", ""))) {
                            progress.dismiss();
                            Intent i = new Intent(Verif_activity.this, Credentials.class);
                            startActivity(i);
                        }
                        //Perform processing based on event values
                    },Throwable ->{
                            Log.d("fuori","stofuori");
                            }
                    );
                    */
    }
});
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:

                i = new Intent(this, com.example.idm.Credentials.class);
                startActivity(i);
                break;

            case R.id.nav_identity:
                i = new Intent(this, Identity_view.class);
                startActivity(i);
                break;
            case R.id.nav_settings:
                i = new Intent(this, Settings.class);
                startActivity(i);
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}