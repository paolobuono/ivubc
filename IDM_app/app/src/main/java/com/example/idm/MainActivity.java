package com.example.idm;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.infura.InfuraHttpService;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.Provider;
import java.security.Security;
import java.util.Calendar;
import java.util.Optional;
import java.util.concurrent.Future;

public class MainActivity extends AppCompatActivity {
    String contractAddress="0xeCB429D2c41f5410e55B351d221b17AF69FC0827";
    DatePickerDialog dob_picker;
    EditText dob,name,surname,mail,mobile,pass,passconf;
    RadioGroup radiogroup;
    RadioButton radioSexButton;
    Button submit;
    Boolean all_ok;
    Web3j web3=Web3j.build(new InfuraHttpService("https://kovan.infura.io/v3/030c2d8a20e54679b2cde8e33b03cd32"));
    final BigInteger gasLimit= BigInteger.valueOf(5500000);
    final BigInteger gasPrice= BigInteger.valueOf(20_000_000_000L);
    ContractGasProvider contractGasProvider = new DefaultGasProvider();
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sendFundsToAccount(String recipientAddress) throws IOException, InterruptedException {
        final Credentials credentials=Credentials.create("0811c0b1de411e956bbd9c51d8496ffee2e07eefec8c6e6ec750066d49086377");
        EthGetTransactionCount ethGetTransactionCount = web3.ethGetTransactionCount(credentials.getAddress(), DefaultBlockParameterName.LATEST).send();
        BigInteger nonce =  ethGetTransactionCount.getTransactionCount();
        BigInteger value = Convert.toWei("0.01", Convert.Unit.ETHER).toBigInteger();
        RawTransaction rawTransaction  = RawTransaction.createEtherTransaction(
                nonce,
                gasPrice,
                gasLimit,
                recipientAddress,
                value);
        byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signedMessage);
        EthSendTransaction ethSendTransaction = web3.ethSendRawTransaction(hexValue).send();
        String transactionHash = ethSendTransaction.getTransactionHash();
        Optional<TransactionReceipt> transactionReceipt = null;
        do {
            EthGetTransactionReceipt ethGetTransactionReceiptResp = web3.ethGetTransactionReceipt(transactionHash).send();
            transactionReceipt = ethGetTransactionReceiptResp.getTransactionReceipt();

            Thread.sleep(3000); // Retry after 3 sec
        } while(!transactionReceipt.isPresent());
        Log.d("prova",transactionHash);
    }

    private void setupBouncyCastle() {
        final Provider provider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
        if (provider == null) {
            // Web3j will set up the provider lazily when it's first used.
            return;
        }
        if (provider.getClass().equals(BouncyCastleProvider.class)) {
            // BC with same package name, shouldn't happen in real life.
            return;
        }
        // Android registers its own BC provider. As it might be outdated and might not include
        // all needed ciphers, we substitute it with a known BC bundled in the app.
        // Android's BC has its package rewritten to "com.android.org.bouncycastle" and because
        // of that it's possible to have another BC implementation loaded in VM.
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
        Security.insertProviderAt(new BouncyCastleProvider(), 1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = MainActivity.this.getSharedPreferences("cr",Context.MODE_PRIVATE);
        if(preferences.getBoolean("registered",false)){
            Intent i=new Intent(MainActivity.this,com.example.idm.Credentials.class);
            startActivity(i);
        }
        setContentView(R.layout.activity_main);
        String walletDirectory = getApplicationContext().getFilesDir().toString();
        setupBouncyCastle();
        dob=(EditText) findViewById(R.id.input_dob);
        dob.setInputType(InputType.TYPE_NULL);
        name=(EditText) findViewById(R.id.input_name);
        surname=(EditText) findViewById(R.id.input_surname);
        mail=(EditText) findViewById(R.id.input_email);
        mobile=(EditText) findViewById(R.id.input_mobile);
        radiogroup=(RadioGroup) findViewById(R.id.radiogroup);
        pass=(EditText) findViewById(R.id.pass);
        passconf=(EditText) findViewById(R.id.passconf);
        submit=(Button) findViewById(R.id.btn_signup);
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                final int day = cldr.get(Calendar.DAY_OF_MONTH);
                final int month = cldr.get(Calendar.MONTH);
                final int year_ = cldr.get(Calendar.YEAR);
                // date picker dialog
               dob_picker = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                if(year>year_||year==year_&&monthOfYear>month||year==year_&&month==monthOfYear&&dayOfMonth>day){dob.setError(getString(R.string.error_dob));
                                dob.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
                                }
                            }
                        }, year_, month, day);
                dob_picker.show();
            }
        });
submit.setOnClickListener(new View.OnClickListener() {
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        all_ok = true;
        if (name.getText().length() == 0) {
            all_ok = false;
            name.setError(getString(R.string.err_name));
            name.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }
        if (surname.getText().length() == 0) {
            all_ok = false;
            surname.setError(getString(R.string.err_surn));
            surname.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }
        if (mail.getText().length() == 0) {
            all_ok = false;
            mail.setError(getString(R.string.err_mail));
            mail.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }
        if (mobile.getText().length() != 10) {
            all_ok = false;
            mobile.setError(getString(R.string.err_mobile));
            mobile.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }
        if (String.valueOf(radiogroup.getCheckedRadioButtonId()).equals("-1")) {
            all_ok = false;
            Toast.makeText(getApplicationContext(), "Seleziona il tuo sesso", Toast.LENGTH_LONG).show();
        }
        if (dob.getText().length() == 0) {
            all_ok = false;
            dob.setError(getString(R.string.err_dob));
            dob.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }
        if (pass.getText().length() == 0) {
            all_ok = false;
            pass.setError(getString(R.string.err_pass));
            pass.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }
        if (!pass.getText().toString().equals(passconf.getText().toString())) {
            all_ok = false;
            passconf.setError(getString(R.string.err_passconf));
            passconf.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
        }

        if (all_ok) {
            submit.setText("Caricamento...");
            String walletName = "";
            String gender;
            radioSexButton=(RadioButton)findViewById(radiogroup.getCheckedRadioButtonId());
            gender=radioSexButton.getText().toString();
            Credentials credentials = null;
            try {
                walletName = WalletUtils.generateNewWalletFile(pass.getText().toString(), new File(walletDirectory));
                credentials = WalletUtils.loadCredentials(pass.getText().toString(), walletDirectory + "/" + walletName);
            } catch (Exception e) {
            }
            Credentials finalCredentials = credentials;
            new Thread(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                public void run() {
                    Credentials credentials_funded=Credentials.create("0811c0b1de411e956bbd9c51d8496ffee2e07eefec8c6e6ec750066d49086377");
                    final IDMContract idm = IDMContract.load(contractAddress, web3,credentials_funded, gasPrice,gasLimit);
                    Future<TransactionReceipt> transactionReceipt =idm.createUser(name.getText().toString(),surname.getText().toString(), pass.getText().toString(),mobile.getText().toString(), gender, mail.getText().toString(), dob.getText().toString(), "did:ethr:" + finalCredentials.getAddress()).sendAsync();
                    try {
                        String result = transactionReceipt.get().getTransactionHash() + " " + transactionReceipt.get().getGasUsed();
                        Log.d("prova", result);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(transactionReceipt.isDone()) {
                        Intent i = new Intent(MainActivity.this, com.example.idm.Credentials.class);
                        startActivity(i);
                    }
                }
            }).start();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("registered",true);
            editor.putString("password",pass.getText().toString());
            editor.putString("address",finalCredentials.getAddress());
            editor.putString("walletName",walletName);
            editor.apply();
        }

    }
});
}
}